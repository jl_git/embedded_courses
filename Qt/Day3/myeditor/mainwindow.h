#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTextEdit>
#include <QLabel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionOpen_triggered();

    void on_actionClose_triggered();

    void on_actionNew_triggered();

    void on_actionSave_triggered();

    void on_cursorPosChanged();

private:
    Ui::MainWindow *ui;
    QString filename;
    QTextEdit* textEdit;
    QLabel* posLabel;
};

#endif // MAINWINDOW_H
