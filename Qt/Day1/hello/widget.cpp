#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    ui->hideButton->setText("隐藏");
    ui->showButton->setText("显示");
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_hideButton_clicked()
{
    ui->label->hide();
}

void Widget::on_showButton_clicked()
{
    ui->label->show();
}
