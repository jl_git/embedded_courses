#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QPushButton>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_hideButton_clicked();
    void on_showButton_clicked();

private:
    Ui::Widget *ui;
    QPushButton *button;
};

#endif // WIDGET_H
