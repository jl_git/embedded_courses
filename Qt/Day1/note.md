## 什么是Qt？

C++开发的跨平台的应用开发框架

- 官网：www.qt.io
- Wiki：wiki.qt.io
- 微信公众号：Qt软件
- 发音：cute

## Qt版本

- 商业：支持嵌入式开发，不需要开源。
- 开源：使用GPL和LGPL协议，使用动态库不需要开源，使用GPL的组件需要开源。

## qmake
```
hello.pro --qmake--> Makefile --make--> 可执行文件
              |----> VC的工程文件 
```
通过qmake生成Makefile
```bash
qmake -project #qmake扫描当前目录中的源文件，生成以当前目录名命名的pro文件
qmake temp.pro #qmake读取pro文件，在当前目录下生成Makefile
make  #make读取Makefile编译代码
```
pro.user文件，是QtCreator生成的，记录了程序编译和运行时需要的环境信息，只跟具体的编译环境相关，打包代码的时候应该删除。
QtCreator在编译程序时会创建单独的build目录，保存生成中间文件和可执行程序。

uic（UI编译器）把UI文件编译为C++代码，自动生成创建控件的代码。
```
QtCreator设计器 --> hello.ui --uic--> ui_widget.h --> 在widget.h中包含
```

## 信号槽机制（重点）
moc（元对象编译器，处理信号槽语法）对使用信号槽机制的类进行预编译。
```
widget.cpp --moc--> moc_widget.cpp --g++--> moc_widget.o --ld--> hello
```

连接信号槽的方式：
1. 通过界面编辑器，鼠标拖动连接对应控件的信号和槽函数，uic生成的代码中通过connect函数连接对应的信号和槽。（不需要写代码，但不能自定义槽函数）
2. 通过界面编辑器，右键菜单“转到槽”按照规则生成槽函数，uic生成的代码中通过connectSlotsByName函数连接对应的信号和槽。（可以自己定义槽函数）
3. 在代码中通过connect函数连接自定义的信号和槽。

从QObject继承的子类定义中，需要增加Q_OBJECT宏，告诉moc需要对此类型进行预处理，支持信号槽。

所有控件和图形界面相关的类，都是从QWidget继承的。

如果自定义的类要支持信号槽机制，需要继承QObject。

从QObject继承的类型，如果在创建对象的时候指定了父对象，当父对象析构的时候会自动调用子对象的析构函数。

作业：
用一个按钮实现显示和隐藏文字，第一次按下后隐藏文字，并将按钮的文字修改为“显示”，再次按下后，显示文字。
